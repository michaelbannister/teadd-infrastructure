resource "google_service_account" "gitlab-staticman" {
  account_id   = "gitlab-staticman"
  display_name = "gitlab-staticman"
}

resource "google_service_account" "staticman" {
  account_id   = "staticman"
  display_name = "staticman"
  description  = "staticman runtime"
}

resource "google_artifact_registry_repository" "docker" {
  provider = google-beta

  location = "europe"
  repository_id = "docker"
  description = "Docker registry for teadd-static project"
  format = "DOCKER"
}

resource "google_artifact_registry_repository_iam_member" "gitlab-staticman" {
  provider = google-beta

  location = google_artifact_registry_repository.docker.location
  repository = google_artifact_registry_repository.docker.name
  role = "roles/artifactregistry.writer"
  member = "serviceAccount:${google_service_account.gitlab-staticman.email}"
}

resource "google_service_account_iam_binding" "staticman_serviceAccountUser" {
  service_account_id = google_service_account.staticman.name
  role               = "roles/iam.serviceAccountUser"
  members = [
    "serviceAccount:${google_service_account.gitlab-staticman.email}",
  ]
}


resource "google_cloud_run_service" "staticman" {
  name     = "staticman"
  location = "europe-west1"
  template {
    spec {
      containers {
        image = "europe-docker.pkg.dev/teadd-static/docker/staticman@sha256:0fdd0192f7311f6df6ce2b6f9134b005f6b9f1ab764b09d9d4ec32445a45c0d5"
        env {
          name = "SECRET_VERSION_NAME"
          value = "projects/teadd-static/secrets/staticman-production-config/versions/latest"
        }
        env {
          name = "NODE_ENV"
          value = "production"
        }
        resources {
          limits = {
            memory = "256Mi"
            cpu    = "1000m"
          }
        }
      }
      service_account_name = google_service_account.staticman.email
    }
    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "5"
        "run.googleapis.com/client-name"   = "terraform"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
  autogenerate_revision_name = true
}
