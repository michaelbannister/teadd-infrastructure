terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~> 3.44"
    }
    google-beta = {
      source = "hashicorp/google-beta"
      version = "~> 3.44"
    }
  }
}

provider "google" {
  project = "teadd-static"
}

provider "google-beta" {
    project = "teadd-static"
}
